package ch.fhnw.masam;

public class Util {

	public static double getPercentage(int population, int cases) {
		return cases*100.0 / population;
	}
	
}

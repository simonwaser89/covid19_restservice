package ch.fhnw.masam;

import java.util.List;
/**
 * This is the covid data service class
 * autor simon waser
 */
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CovidService {
	
	/** 
	 * Resporitory attribute which defines queries
	 */
	
	private static final Logger LOG = LogManager.getLogger(CovidService.class);
	
	@Autowired
    private CovidRepository covidRepository;
	
	/**
	 * Query for a specific data.
	 * @param date Given date.
	 * @return Returns a list of all cases for the given date.
	 */
    
    public List<CovidCase> findByDate(String date) {
    	return covidRepository.findByDate(date);
    }
    
    public List<CovidCase> findByCountry(String country) {
    	LOG.debug("Debug Message in findByCountry");
    	LOG.fatal("Fatal Message in findByCountry");
    	return covidRepository.findByCountry(country);
    }
    
    public List<String> getAllCountries() {
    	return covidRepository.getAllCountries();
    }
	
}

package ch.fhnw.masam;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class UtilTestJU5 {

	@Test
	public void calculationTest() {
		int population = 100;
		int cases = 10;
		assertEquals(Util.getPercentage(population, cases), 10);
	}
	
}
